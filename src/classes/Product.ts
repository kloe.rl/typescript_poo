// Création d'un Type personnalisé 'dimensions'
export type Dimension = {
    length: number;
    width: number; 
    height: number;
};

// Création d'une class Product et d'un constructor. 
// Tous les membres sont public
export class Product {
    productId: number;
    name: string;
    weight: number;
    price: number;
    // QUESTION pourquoi ça affiche "dimensions: [Object]" ?
    dimensions: Dimension;

    // Méthode constructor qui permet de créer et d'initialiser des objets
    constructor(productId: number, name: string, weight: number, dimensions: Dimension, price: number) {
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.dimensions = dimensions;
        this.price = price;
    }
    // Méthode qui affiche les membres d'un objet de la classe Product
    displayDetails() {
        return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Dimensions: length : ${this.dimensions.length}, width : ${this.dimensions.width}, height : ${this.dimensions.height} Price: ${this.price} €`
    }

};