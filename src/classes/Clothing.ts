import { Product, Dimension } from './Product'
import { ClothingSize } from '../enums/ClothingSize'

export class Clothing extends Product {
    size: ClothingSize;

    // Construction qui récupère les propriétés de la classe parent 'Product' 
    constructor(productId: number, name: string, weight: number, dimensions: Dimension, size: ClothingSize, price: number) {
        super(productId, name, weight, dimensions, price);
        this.size = size;
    }
}