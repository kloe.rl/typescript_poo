// Création d'un Type personnalisé 'adsress
export type Address = {
    street: string;
    city: string;
    postalCode: string;
    country: string;
};

// Création d'une class Customer et d'un constructor. 
// Tous les membres sont public
export class Customer {
    // L'accès par défaut des membres est 'public'
    // Les champs sont déclarés directement dans le constructeur
    address: Address | undefined;

    // Méthode constructor qui permet de créer et d'initialiser un objet de la class Customer
    // Les champs sont déclarés directement dans le constructeur grâce à la mention 'public'
    constructor(public customerId: number, public name: string, public email: string){

    };

    // Méthode qui permet de renseigner l'adresse du client 
    addAddress(street: string, postalCode: string, city: string, country: string) {
        this.address = {
            street : street,
            postalCode : postalCode,
            city : city,
            country : country
        }
    }

    // Méthode qui affiche les valeurs des membres d'un objet créé par le constructor
    displayInfo(): string {
        return `Customer ID : ${this.customerId}, Name : ${this.name}, Email : ${this.email}, Address : ${this.displayAddress()}`
    }
    // Méthode qui affiche l'adresse d'un client
    displayAddress(): string {
        if (this.address) {
            return `Address : ${this.address.street}, ${this.address.postalCode}, ${this.address.city}, ${this.address.country}`
        } else {
            return "No address found"
        }
    }
};