import { Deliverable } from "../interfaces/Deliverable";

export class ExpressDelivery implements Deliverable {
    estimateDeliveryTime(weight: number): number {
        let deliveryTime: number = 0;
        if (weight <= 5) {
            deliveryTime = 1
        } else {
            deliveryTime = 3
        }
        return deliveryTime
    };
    calculateShippingFees(weight: number): number {
        let shippingFee: number = 0;
        if (weight < 1) {
            shippingFee = 8
        } else if (weight >= 1 && weight < 5) {
            shippingFee = 14
        } else {
            shippingFee = 30
        }
        return shippingFee
    }
}