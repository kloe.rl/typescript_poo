import { ShoeSize } from '../enums/ShoeSize'
import { Product, Dimension } from './Product'

export class Shoes extends Product {
    size: ShoeSize;

    constructor(productId: number, name: string, weight: number, size: ShoeSize, dimensions: Dimension, price: number) {
        super (productId, name, weight, dimensions, price )
        this.size = size;
    }
};