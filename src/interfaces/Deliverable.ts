export interface Deliverable {
    // Méthode pour estimer le délai de livraison en nombre de jours
    estimateDeliveryTime(weight: number): number 

    // Méthode pour calculer les frais de livraison
    calculateShippingFees(weight: number): number
}