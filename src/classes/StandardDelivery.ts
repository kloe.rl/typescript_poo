import { Deliverable } from "../interfaces/Deliverable";

export class StandardDelivery implements Deliverable {

    estimateDeliveryTime(weight: number): number {
        let deliveryTime: number = 0;
        if (weight < 10) {
            deliveryTime = 7
        } else {
            deliveryTime = 10
        }
        return deliveryTime;
    };

    calculateShippingFees(weight: number): number {
        let shippingFee: number = 0;
        if (weight < 1) {
            shippingFee = 1
        } else if (weight >= 1 && weight < 5) {
            shippingFee = 10
        } else {
            shippingFee = 20
        }
        return shippingFee;
    }
};