import { Customer } from './classes/Customer'
import { Product } from './classes/Product'
import { Clothing } from './classes/Clothing';
import { ClothingSize } from './enums/ClothingSize';
import { Shoes } from './classes/Shoes';
import { ShoeSize } from './enums/ShoeSize';
import { Order } from './classes/Order';
import { ExpressDelivery } from './classes/ExpressDelivery';
import { StandardDelivery } from './classes/StandardDelivery';


const titouan = new Customer(43, 'Titouan', 'titouandu32@getMaxListeners.com');
titouan.addAddress("Salamèche","31500","Bourg Palette", "Kantô");
// console.log(titouan.displayAddress());
// console.log(titouan.displayInfo());

const patère = new Product(1, 'Patère des neiges', 1.2, { length: 100, width: 30, height: 30 }, 45);
// console.log(patère.displayDetails());
// console.log(patère.dimensions);

const pantacourt = new Clothing(2,"pantacourt", 0.85,{length: 200, width: 60, height: 3 }, ClothingSize.M, 26.99);
// console.log(pantacourt.displayDetails());

const charentaise = new Shoes(3, "charentaise", 0.450, ShoeSize.EU46, { length: 40, width: 12, height: 7 }, 74.99);
// console.log(charentaise.displayDetails());

const order1 = new Order(1, titouan);

// AJOUTE DES PRODUITS A LA COMMANDE
order1.addProduct(patère);
order1.addProduct(pantacourt);
order1.addProduct(charentaise);

// SUPPRIME LE PRODUIT #3
// order1.deleteProduct(3);

// console.log("DELETED ID 3: ", order1);
// console.log("Total weight :", order1.calculateWeight());
// console.log("Total cost :", order1.calculateTotal());
// console.log("Display Order :", order1.displayOrder());

//CREE UNE NOUVELLE INTERFACE DE LIVRAISON Express
const expressShipping = new ExpressDelivery;

// ASSIGNE UN TYPE DE LIVRAISON A LA COMMANDE
order1.setDelivery(expressShipping)

// CALCUL DES COUT ET DELAIS DE LIVRAISON POUR Express
console.log("Shipping Fees EX :",order1.calculateShippingFees())
console.log("Delivery Time EX :",order1.estimateDeliveryTime())

// CREE UNE NOUVELLE INTERFACE DE LIVRAISON Standard
// const standardShipping = new StandardDelivery;

// ASSIGNE UN TYPE DE LIVRAISON A LA COMMANDE
// order1.setDelivery(standardShipping);

// CALCUL DES COUT ET DELAIS DE LIVRAISON POUR Standard
// console.log("Shipping Fees ST :",order1.calculateShippingFees())
// console.log("Delivery Time ST :",order1.estimateDeliveryTime())
