import { Customer } from './Customer'
import { Product } from './Product'
import { Deliverable } from '../interfaces/Deliverable';

export class Order {
    orderId: number;
    customer: Customer;
    productList: Product[];
    orderDate: Date;
    delivery: Deliverable | undefined;

    // Lors de la création je souhaite renseigner uniquement le client et l'ID de la commande
    constructor(orderId: number, customer: Customer) {
        this.orderId = orderId;
        this.customer = customer;
        this.productList = [];
        // Je souhaite que la date soit créée au moment de l'instanciation et corresponde au moment de l'instanciation
        this.orderDate = new Date();
    };
    //Méthode qui ajoute un produit à la commande
    addProduct(product: Product) {
        // On ajoute le produit au tableau vide
        this.productList.push(product)
        return `${product} has been added to the basket. Your basket contains : ${this.productList}`
    };
    // Méthode qui supprime un produit de la commande
    deleteProduct(productId: number) {
        // Grâce à un .filter on ne garde que les produits dont l'ID ne correspond pas au produit que l'on souhaite supprimer
        this.productList = this.productList.filter(product => product.productId !== productId)
        return `The product ID #${productId} has been deleted succesfully.`
    };
    // Méthode qui calcule le poids total de la commande
    calculateWeight() {
        let totalWeight: number = 0;
        // On additionne les poids de chaque produit grâce à une boucle
        for (const product of this.productList) {
            totalWeight += product.weight
        }
        return totalWeight;
        // return `Total weight for the order ID#${this.orderId} is ${totalWeight} kg.`
    };
    // Méthode qui calcule le prix total de la commande
    calculateTotal() {
        let totalCost: number = 0;
        // On additionne les prix de chaque produit grâce à une boucle
        for (const product of this.productList) {
            totalCost += product.price
        }
        return `Total cost for the order ID#${this.orderId} is ${totalCost} PokeDollars`
    };
    // Méthode qui affiche les détails de la commande : les informations de l'utilisateur, les informations de chaque produit et le total de la commande.
    displayOrder() {
        // Récupèration du résultat de la méthode calculateTotal
        let totalCost = this.calculateTotal()
        // Création d'un tableau avec toutes nos données qu'on affichera ensuite
        let order: any[] =
            [
                this.orderId,
                this.customer,
                this.productList,
                totalCost
            ]
        return order
    };

    // Méthode qui permet d'assigner un service de livraison à la commande
    setDelivery(delivery: Deliverable) {
        this.delivery = delivery
    };

    // Méthode qui permet de calculer les frais de livraison en fonction du poids
    calculateShippingFees() {
        // Récupération du résultat de la méthode qui calcule le poids total
        const totalWeight: number = this.calculateWeight();
        // Appel de la méthode qui calcule les frais de livraison en fonction du mode de livraison choisie
        return this.delivery?.calculateShippingFees(totalWeight);
    };

    // Méthode qui permet de calculer le délai de livraison
    estimateDeliveryTime() {
        // Récupération du résultat de la méthode qui calcule le poids total
        const totalWeight: number = this.calculateWeight();
        // Appel de la méthode qui calcule les délais de livraison en fonction du mode de livraison choisie
        return this.delivery?.estimateDeliveryTime(totalWeight);
    };
};